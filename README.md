# conception-dev-platform

L'objectif du projet sera de faire toute la conception et l'implémentation de la couche data d'une application de mise en relation entre des développeur⋅euses et des porteur⋅euses de projets.

## Attendus

### Conception
* Réaliser un diagramme de Use Case pour lister identifier les acteurs de l'application et lister les fonctionnalités
* Ecrire 3-4 use cases détaillés sur [ce modèle là](https://docs.google.com/document/d/1cP0c8fIvBOYzYJY_ZYOQSI79H8ZqFE5T8Tv9ud_BT1s/edit?usp=sharing)
* Réaliser un diagramme d'activité pour détailler le scénario de 2-3 fonctionnalités
* Identifier les entités et réaliser un diagramme de classe pour les représenter et leurs relations
* Créer un diagramme de classes des différentes couches du backend dans le cas d'une API Rest

### Implémentation
* Créer les entités de l'application
* Créer les différents DAO (avec du générique ou non)